var tabs = (function () {

    function init() {
        $('.js-b_wrap-tabs .b_tabs_elem').on('click', function() {
            var $this = $(this);
            if($this.hasClass('-active')) {
                return;
            } else {
                $('.js-b_wrap-tabs .b_tabs_elem').attr('class', 'b_tabs_elem');
                $this.addClass('-active');
                var elemNumber = $this.data('seq');
                var newContentElem = $('.b_tabs-content_elem').eq(elemNumber);
                $('.b_tabs-content_elem').attr('class', 'b_tabs-content_elem');
                newContentElem.addClass('-show');
            }
        })
    }

    return {
        func: init()
    }

})();