var adNav = (function () {

    function init() {
        $(document).on('scroll', function(){
            if($(document).scrollTop() > $('.b_header').height()) {
                $('.b_main-nav').addClass('-fixed');

            } else if ( $(document).scrollTop() < $('.b_header').height()){
                $('.b_main-nav').removeClass('-fixed');
            }
        })
    }

    return {
        func: init()
    }

})();