var timetable = (function () {

    function init() {
        $('.b_timetable_tb-cell.-reserved').on('click', function () {
            if ($('.popover_container')) {
                $('.popover_container').detach();
                var $this = $(this);
                var popoverContent = $this.find('.b_timetable_cell_content_popover-content').html();


                if ( $(this).parent().width() - $(this).width() * 3 <  $(this).position().left && $(document).width() > 748 ) {
                    var positionX = $this.position().left - 289;
                    var positionY = $this.position().top - 13;
                    var positionText = "-left";
                } else if ( $(this).parent().width() - $(this).width() * 3 >  $(this).position().left && $(document).width() > 748) {
                    var positionX = $this.position().left + $this.outerWidth() + 13;
                    var positionY = $this.position().top - 13;
                    var positionText = "-right";
                } else if ( $(".b_timetable").hasClass('-mobile') ) {
                    var positionX = 0;
                    var positionY = $this.position().top + 82;
                    var positionText = "-top";
                }


                var popoverMarkup = '<div class=\'popover_container ' + positionText + '\'>' +
                    '<div class=\'popover-close\'><i class=\'fa fa-times\'></i></div>' +
                    '<div class=\'popover_body\'>' +
                    popoverContent +
                    '</div>' +
                    '</div>';

                $('.b_timetable').append( popoverMarkup );

                $('.popover_container').css({top:positionY, left:positionX});
            };
        });
        $('body').on('click', '.popover-close', function (){
            $('.popover_container').detach();
        });

    }

    return {
        func: init()
    }

})();