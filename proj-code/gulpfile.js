//npm install gulp browser-sync gulp-load-plugins gulp-cssmin gulp-rename gulp-sass gulp-autoprefixer gulp-imagemin imagemin-pngquant gulp-rigger gulp-sourcemaps

var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    $ = require('gulp-load-plugins')(),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rigger = require('gulp-rigger'),
    sourcemaps = require('gulp-sourcemaps');

var path = {
    thisProj: {
        src: {
            html: 'develop/pages/*.html',
            js: 'develop/js/**/**/**/*',
            css: 'develop/scss/**/*.scss',
            img: 'develop/img/**/*.*',
            fonts: 'develop/fonts/**/*.*'
        },
        build: {
            html: 'production/pages/',
            js: 'production/js/',
            css: 'production/css/',
            img: 'production/img/',
            fonts: 'production/fonts/'
        }
    }
};

gulp.task('html' ,function () {
    gulp.src(path.thisProj.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.thisProj.build.html));
});
gulp.task('sass', function () {
    gulp.src(path.thisProj.src.css)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: true
        }))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.thisProj.build.css))
});
gulp.task('img', function () {
    gulp.src(path.thisProj.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.thisProj.build.img));
});
gulp.task('fonts', function () {
    gulp.src(path.thisProj.src.fonts)
        .pipe(gulp.dest(path.thisProj.build.fonts));
});
gulp.task('js', function () {
    gulp.src(path.thisProj.src.js)
        .pipe(rigger())
        .pipe(gulp.dest(path.thisProj.build.js));
});

gulp.task('serve', ['html', 'sass', 'img', 'fonts', 'js'], function() {
    browserSync({
        notify: false,
        open: false,
        server: {
            baseDir: ['production']
        }
    });
    gulp.watch([path.thisProj.src.html], ['html', browserSync.reload]);
    gulp.watch([path.thisProj.src.css], ['sass', browserSync.reload]);
    gulp.watch([path.thisProj.src.img], ['img', browserSync.reload]);
    gulp.watch([path.thisProj.src.fonts], ['fonts', browserSync.reload]);
    gulp.watch([path.thisProj.src.js], ['js', browserSync.reload]);
});