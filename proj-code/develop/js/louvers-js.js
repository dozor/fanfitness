var louvers = (function () {

    function init() {
        $('.js-louvers').on('click', function () {
            var $this = $(this);
            var parent = $this.parents('.b_short-info-section_main-content');
            var openText = 'Свернуть новость';
            var closedText = 'Показать новость полностью';

            if (parent.hasClass('-open')) {
                parent.removeClass('-open').addClass('-closed');
                $this.text(closedText);
            } else {
                parent.removeClass('-closed').addClass('-open');
                $this.text(openText);
            }
        });
    }


    return {
        func: init()
    }

})();