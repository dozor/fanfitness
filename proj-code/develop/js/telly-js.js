var telly = (function () {

    function init() {
        $('.js-telly .b_telly_channels_elem').on('click', function (){
            var $this = $(this),
                $thisSeq = $this.data('seq'),
                contentZoneElem = $this.parents('.js-telly').find('.b_telly_content-zone_elem'),
                contentZoneCheckedElem = contentZoneElem[$thisSeq];

            $this.parents('.js-telly').find('.b_telly_channels_elem').removeClass('-active');
            contentZoneElem.removeClass('-show');
            $this.addClass('-active');
            $(contentZoneCheckedElem).addClass('-show');
        });
    }

    return {
        func: init()
    }

})();