//= jquery-1.11.3.min.js
//= jquery.bxslider.min.js
// adheres-nav-js.js
//= telly-js.js
//= timetable-js.js
//= tabs-js.js
//= louvers-js.js
//= jquery.tmpl.min.js
//= jquery.easing.1.3.js
//= jquery.elastislide.js
//= gallery.js

$(document).ready(function() {
    $('.big-slider').bxSlider({
        pagerCustom: '#custom-pager'
    });
    $('.little-slider').bxSlider({
        controls: false
    });
    telly.init;
    timetable.init;
    tabs.init;
    louvers.init;
    //adNav.init;
});